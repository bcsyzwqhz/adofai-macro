#include<iostream>
#include<string>
#include<nlohmann/json.hpp>
#include<fstream>

#define endl '\n'
#define PATHDATA 1
#define ANGLEDATA 0

typedef struct adofaiTile
{
    double bpm;
    double angle;
    bool turns;
}Tile;

using namespace std;
using namespace nlohmann;

void getJson(string &path,json &JSON);
bool checkData(json &adofai_map);
void loadMap(json &adofai_map);
