CC=g++
INCLUDE=-I include
FLAG=-std=c++17 -O2 -finput-charset=UTF-8 -fexec-charset=GBK
TARGET=Macro
FILES=main.cpp readmap.cpp macro.hpp
$(TARGET): $(FILES)
	$(CC) $(FILES) $(INCLUDE) $(FLAG) -o $(TARGET)
clean:
	$(RM) $(TARGET)