#include"macro.hpp"

using namespace std;
using namespace nlohmann;

void getJson(string &path,json &JSON)
{
	path.erase(0,path.find_first_not_of(' '));
    path.erase(path.find_last_not_of(' ') + 1);
    ifstream fin(path);
    if(!fin.is_open())
    {
        cout<<"无法打开谱面,请检查文件路径是否正确"<<endl;
        exit(1);
    }
    fin>>JSON;
    cout<<"打开谱面成功！- "<<path<<endl;
}

int main()
{
    cout<<"ADOFAI Macro alpha 1.0"<<endl;
    cout<<"粘贴谱面文件路径到此处:";
    string path;
    getline(cin,path);
    json adofai_map;
    getJson(path,adofai_map);
    cout<<"作曲家:"<<adofai_map["settings"]["artist"]<<endl;
    cout<<"音乐名:"<<adofai_map["settings"]["song"]<<endl;
    cout<<"BPM: "<<adofai_map["settings"]["bpm"]<<endl;
	loadMap(adofai_map); 
    return 0;
}