#include"macro.hpp"

using namespace std;
using namespace nlohmann;

bool checkData(json &adofai_map)
{
	if(adofai_map.contains("pathData"))
		return 1;
	if(adofai_map.contains("angleData"))
		return 0;
}

void loadMap(json &adofai_map)
{
	vector<Tile>mapdata;
	Tile now;
	bool saveChk=checkData(adofai_map);
	string pathdata;
	json angledata;
	if(saveChk==PATHDATA)
		cout<<"角度储存方式："<<"pathData"<<endl;
	if(saveChk==ANGLEDATA)
		cout<<"角度储存方式："<<"angleData"<<endl;
	double basicBPM;
	try
	{
		basicBPM=adofai_map["settings"]["bpm"].get<double>();
	}
	catch(const std::exception& e)
	{
		cerr<<"!! 出错了 !!"<<endl<<"解析谱面时出现错误，详细信息如下："<<endl;
		cerr << e.what() << '\n';
	}
	now.bpm=basicBPM;
	now.angle=0;
	now.turns=0;
	mapdata.push_back(now);
	for(int i=0;i<adofai_map["settings"]["actions"].size();i++)
	{
		try
		{
			json it=adofai_map["settings"]["actions"].at(i);
			if(it["eventType"].contains("SetSpeed"))
			{
				if(it["speedType"].contains("Multiplier"))
				{
					now.bpm=mapdata.back().bpm*it["bpmMultiplier"].get<double>();
				}
				if(it["speedType"].contains("Multiplier"))
				{
					now.bpm=mapdata.back().bpm*it["speedType"].get<double>();
				}
			}
		}
		catch(const std::exception& e)
		{
			cerr<<"!! 出错了 !!"<<endl<<"解析谱面时出现错误，详细信息如下："<<endl;
			std::cerr << e.what() << '\n';
		}
		
	}
}